const chatForm = document.getElementById("chat-form");
const chatMsg = document.querySelector(".chat-messages");
const socket = io();
const roomName = document.getElementById('room-name');
const userList = document.getElementById('users');

const { username, room } = Qs.parse(location.search, {
  ignoreQueryPrefix: true
});

socket.emit("joinChatRoom", { username, room });

socket.on("roomUser", ({ room, users }) => {
  outputRoomName(room); 
  outputUsers(users); 
});

socket.on("msg", msg => {
  outputMessage(msg);
  //Scroll down
  chatMsg.scrollTop = chatMsg.scrollHeight;
});

chatForm.addEventListener("submit", e => {
  e.preventDefault();
  const msg = e.target.elements.msg.value;
  socket.emit("chatMsg", msg);
  e.target.elements.msg.value = "";
  e.target.elements.msg.focus();
});

function outputMessage(msg) {
  const div = document.createElement("div");
  div.classList.add("message");
  div.innerHTML = `
  <p class="meta">
    <span>${msg.username}</span>  <span>${msg.time}</span>  
  </p>
  <p class="text">
    ${msg.txt}
  </p>`;
  document.querySelector(".chat-messages").appendChild(div);
}


function outputRoomName(room){
  roomName.innerText  = room;
}

function outputUsers(users){

    userList.innerHTML = `
      ${users.map(user => `<li>${user.username}</li>`).join('')}
    `;
}
