const path = require("path");
const express = require("express");
const http = require("http");
const socketio = require("socket.io");
const formatMessage = require("./utils/messages");
const app = express();
const server = http.createServer(app);
const io = socketio(server);
const chatName = "Chat bot";

const { userJoin , getCurrentUser, userLeave, getRoomUsers } = require("./utils/user");

//set static folder
//on definit un dossier static permet notamment de pointer sur le ficher index.html
app.use(express.static(path.join(__dirname, "public")));

io.on("connection", socket => {
  socket.on("joinChatRoom", ({ username, room }) => {
    const user = userJoin(socket.id, username, room);
    socket.join(user.room);
    //emit only the current user
    socket.emit("msg", formatMessage(chatName, "Welcome to chat tools"));

    //send to every body except the current
    socket.broadcast.to(user.room).emit("msg", formatMessage(user.username, `${user.username} is in the room`));

    io.to(user.room).emit('roomUser', {
        room: user.room,
        users: getRoomUsers(user.room),
    });

  });

  //list new submit msg
  socket.on("chatMsg", msg => {
    const user = getCurrentUser(socket.id);
    io.to(user.room).emit("msg", formatMessage(user.username, msg));
  });

  socket.on("disconnect", () => {
    const user = userLeave(socket.id)
    console.log(user);
    if(user){
        io.to(user.room).emit("msg", formatMessage(chatName,  `${user.username} as leave the chat`));
    }
  });
});

const PORT = 3000 || process.env.PORT;
server.listen(PORT, () => console.log(`server run on ${PORT}`));
